#!/usr/bin/python

import random
import sys
import urllib
import urllib2

USERNAME = "sobchak"
PASSWORD = ""
SOURCE_NAME = "nam"
POST_URL = "http://identi.ca/api/statuses/update.json"

QUOTES_STRING = """Do you see what happens, @Larry? DO YOU SEE WHAT HAPPENS WHEN YOU FUCK A STRANGER IN THE ASS?
Oh, come on @Donny; they were threatening castration! Are we gonna split hairs here? Am I wrong?
Say what you will about the tenets of National Socialism, @Dude, but at least it's an ethos.
OVER THE LINE! ... I'm sorry, @Smokey. You were over the line, that's a foul.
Uh, excuse me. Mark it zero. Next frame.
@Smokey, this is not 'Nam. This is bowling. There are rules.
Saturday, @Donny, is Shabbos, the #Jewish day of rest. That means that I don't work, I don't get in a car, I don't fucking ride in a car, I don't pick up the phone, I don't turn on the oven, and I sure as shit don't fucking roll! Shomer shabbos!
#Shomer-shabbos!
That rug really tied the room together, did it not?
Shut the fuck up, @Donny.
@Donny, you're out of your element!
No, @Donny, these men are nihilists, there's nothing to be afraid of.
Am I wrong?
Am I wrong?
Am I wrong?
Am I wrong?
Am I wrong?
Am I wrong?
Am I wrong?
Am I wrong?
Am I wrong?
Am I wrong?
Am I wrong?
Am I wrong?
AM I WRONG?
Fuck it, Dude. Let's go bowling.
@bkuhn, Calmer than you are.
@bkuhn, Calmer than you are.
@bkuhn, Calmer than you are.
@bkuhn, Calmer than you are.
@bkuhn, Calmer than you are.
@bkuhn, Calmer than you are.
You are entering a world of pain.
@evan, I myself dabbled in pacifism once, not in 'Nam of course.
Also, Dude; #Chinaman is not the preferred nomenclature. #AsianAmerican, please.
@baughj, Life does not stop and start at your convenience, you miserable piece of shit.
You want a toe? I can get you a toe, believe me.
Hell, I can get you a toe by 3 o'clock this afternoon... with nail polish. These fucking amateurs...
Who's in charge of scheduling?
I told that kraut a fucking thousand times that I don't roll on Shabbos! 
Three thousand years of beautiful tradition, from Moses to Sandy Koufax... 
You're goddamn right I'm living in the fucking past! 
Huh? No, what the fuck are you... I'm not... 
@rejon, We're talking about unchecked aggression here, dude.
Fuck you. Fuck the three of you. 
Without a hostage, there is no ransom. That's what ransom is. Those are the fucking rules. 
WHO'S THE FUCKING NIHILIST HERE! WHAT ARE YOU, A BUNCH OF FUCKING CRYBABIES? 
You mark that frame an 8, and you're entering a world of pain. 
Has the whole world gone crazy? Am I the only one around here who gives a shit about the rules?
You have got to buck up, man. You cannot drag this negative energy in to the tournament!
8 year olds, dude."""
QUOTES = QUOTES_STRING.split("\n")

quote = QUOTES[random.randrange(0, len(QUOTES))]

passman = urllib2.HTTPPasswordMgrWithDefaultRealm()
passman.add_password(None, POST_URL, USERNAME, PASSWORD)
authhandler = urllib2.HTTPBasicAuthHandler(passman)
opener = urllib2.build_opener(authhandler)
urllib2.install_opener(opener)
pagehandle = urllib2.urlopen(POST_URL,
                             data=urllib.urlencode({'status': quote}))
